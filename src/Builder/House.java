package Builder;

/////////////////////////////////////////////////////////////
///////// I did it as shown in the lecture, /////////////////
///////// because didn't understand the method //////////////
///////// that implemented in the website ///////////////////
/////////////////////////////////////////////////////////////


public class House {
    private int windows;
    private int doors;
    private int rooms;
    private boolean hasGarage;
    private boolean hasSwimPool;
    private boolean hasStatues;
    private boolean hasGarden;

    private House(HouseBuilder house){
        this.windows = house.windows;
        this.doors = house.doors;
        this.rooms = house.rooms;
        this.hasGarage = house.hasGarage;
        this.hasSwimPool = house.hasSwimPool;
        this.hasStatues = house.hasStatues;
        this.hasGarden = house.hasGarden;
    }

    public String toString(){
        return "Information: windows = "+windows+", doors = "+doors+", rooms = "+ rooms+", hasGarage = "+hasGarage+", hasSwimPool = "+hasSwimPool+", hasStatues = "+hasStatues+", hasGarden = "+hasGarden;
    }

    public static class HouseBuilder{
        private int windows;
        private int doors;
        private int rooms;
        private boolean hasGarage;
        private boolean hasSwimPool;
        private boolean hasStatues;
        private boolean hasGarden;

        public HouseBuilder (int windows, int doors, int rooms){
            this.doors = doors;
            this.windows = windows;
            this.rooms = rooms;
        }

        public HouseBuilder hasGarage(boolean a){
            this.hasGarage = a;
            return this;
        }
        public HouseBuilder hasSwimPool(boolean a){
            this.hasSwimPool = a;
            return this;
        }
        public HouseBuilder hasStatues(boolean a){
            this.hasStatues = a;
            return this;
        }
        public HouseBuilder hasGarden(boolean a){
            this.hasGarden = a;
            return this;
        }

        public House build(){
            House house = new House(this);
            return house;
        }

    }

    public static void main(String[] args) {
        House house = new HouseBuilder(4, 2,3)
                .hasGarage(true)
                .build();
        System.out.println(house);
    }
}
