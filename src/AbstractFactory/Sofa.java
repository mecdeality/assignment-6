package AbstractFactory;

public interface Sofa {
    boolean hasLegs();
    boolean hasArmrest();
    void deliver();
    String toString();
}
