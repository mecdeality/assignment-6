package AbstractFactory;

public interface CoffeeTable {
    boolean hasLegs();
    void deliver();
    String toString();
}
