package Factory.method;

public interface Transport {
    public void deliver();
}
