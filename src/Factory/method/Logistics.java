package Factory.method;

public class Logistics implements LogisticFactory {

    public Transport createTransport(String t) {
        if(t.equalsIgnoreCase("TRUCK")){
            return new Truck();
        }
        else if(t.equalsIgnoreCase("SHIP")){
            return new Ship();
        }return null;
    }

}
