package Factory.method;

public interface LogisticFactory {
    public Transport createTransport(String type);
}